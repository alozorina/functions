const getSum = (str1, str2) => {
    if (typeof str1 !== 'string' || typeof str2 !== 'string') return false;
    if (str1 === '') return str2;
    if (str2 === '') return str1;
    if (!/\d/.test(str1) || !/\d/.test(str2)) return false;

    let str1Num = str1.split('').map(Number);
    let str2Num = str2.split('').map(Number);
    return str1Num.map((x, i) => x + str2Num[i]).join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let posts = listOfPosts.filter(p => p.author === authorName);
    let comments = listOfPosts
        .filter(p => p.comments)
        .map(p => p.comments)
        .flat()
        .filter(c => c.author === authorName);

    return `Post:${posts.length},comments:${comments.length}`;
};

const tickets = (people) => {
    const TICKET_PRICE = 25;
    let numPeople = people.map(Number);
    if (numPeople[0] !== TICKET_PRICE) return 'NO'
    let balance = 0;
    for (let i = 0; i < numPeople.length; i++) {
        if (numPeople[i] - TICKET_PRICE > balance)
            return 'NO';
        if (numPeople[i] === 25)
            balance += 25;
        else
            balance += numPeople[i] - TICKET_PRICE;
    }
    return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
